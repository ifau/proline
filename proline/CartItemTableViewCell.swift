//
//  CartItemTableViewCell.swift
//  proline
//
//  Created by ifau on 21/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class CartItemTableViewCell: UITableViewCell
{
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var quantityLabel: UILabel!
    @IBOutlet var totalPriceLabel: UILabel!
    @IBOutlet var photoImageView: UIImageView!
}
