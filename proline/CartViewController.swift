//
//  CartViewController.swift
//  proline
//
//  Created by ifau on 21/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class CartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    @IBOutlet var clearButton: UIBarButtonItem!
    @IBOutlet var orderButton: UIBarButtonItem!
    
    private var cartItems: [ProductStorageModel] = []
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        reload()
    }
    
    func reload()
    {
        cartItems.removeAll()
        cartItems = DataManager.sharedInstance.getProducts()
        tableView.reloadData()
        
        //title = cartItems.count > 0 ? nil : "Корзина"
        
        clearButton.enabled = cartItems.count > 0 ? true : false
        orderButton.enabled = cartItems.count > 0 ? true : false
        
        clearButton.tintColor = cartItems.count > 0 ? nil : UIColor.clearColor()
        orderButton.tintColor = cartItems.count > 0 ? nil : UIColor.clearColor()
    }
    
    // MARK: - UITableViewDelegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return cartItems.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 120
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        let cartItemCell = cell as! CartItemTableViewCell
        let cartItem = cartItems[indexPath.row]
        
        cartItemCell.titleLabel.text = cartItem.name!
        cartItemCell.priceLabel.text = "\(cartItem.price!.integerValue) руб."
        cartItemCell.quantityLabel.text = "Количество: \(cartItem.quantity!.integerValue)"
        cartItemCell.totalPriceLabel.text = "Стоимость: \(cartItem.price!.integerValue) руб."
        cartItemCell.photoImageView.kf_setImageWithURL(cartItem.pictureURL)
        cartItemCell.photoImageView.kf_showIndicatorWhenLoading = true
        
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        
        let footerLabel = UILabel()
        footerLabel.text = cartItems.count > 0 ? "Общая стоимость: \(cartItems.reduce(0) {$0 + $1.price!.integerValue * $1.quantity!.integerValue}) руб."
                                               : "Корзина пуста"
        
        footerLabel.font = UIFont.systemFontOfSize(13)
        footerLabel.textColor = UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.0)
        footerLabel.textAlignment = NSTextAlignment.Center
        return footerLabel
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 70
    }
    
    // MARK: - UINavigationBar Items Actions
    
    @IBAction func clearButtonPressed(sender: AnyObject)
    {
        let alertController = UIAlertController(title: nil, message: "Вы уверены, что хотите очистить корзину?", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "Очистить", style: UIAlertActionStyle.Default, handler:
        { [unowned self] (UIAlertAction) -> Void in
            
            DataManager.sharedInstance.removeAllProducts()
            self.reload()
        }))
        
        alertController.addAction(UIAlertAction(title: "Отмена", style: UIAlertActionStyle.Cancel, handler: nil))
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func orderButtonPressed(sender: AnyObject)
    {
        performSegueWithIdentifier("orderSegue", sender: nil)
    }
}
