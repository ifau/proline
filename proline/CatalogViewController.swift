//
//  CatalogViewController.swift
//  proline
//
//  Created by ifau on 27/12/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit
import Kingfisher

class CatalogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    var refreshControl: UIRefreshControl!
    
    var parentID : Int = 0
    private var catalog : CatalogResponseModel?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if parentID == 0 { title = "Каталог" }
        
        tableView.delegate = self
        tableView.dataSource = self
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: "updateCatalog", forControlEvents: .ValueChanged)
        tableView.addSubview(refreshControl)
        
        updateCatalog()
    }
    
    // MARK: - UITableViewDelegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if catalog == nil
        {
            return 0
        }
        else // REWRITE THIS SHIT !!!!
        {
            return (section == 0 ? catalog?.categories.count : catalog?.products.count)!
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 44.0
        }
        else
        {
            let product = catalog!.products[indexPath.row]
            let desc = product.describing as NSString!
                
            let sizeOfDesc = desc.boundingRectWithSize(
                    CGSizeMake(self.view.bounds.size.width - 18, CGFloat.infinity),
                    options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                    attributes: [NSFontAttributeName: UIFont.systemFontOfSize(15)],
                    context: nil).size
            
            return 164 + sizeOfDesc.height
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let identifier = indexPath.section == 0 ? "Cell" : "ProductCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if indexPath.section == 0
        {
            cell.textLabel?.text = catalog?.categories[indexPath.row].name
        }
        else
        {
            let productCell = cell as! ProductTableViewCell
            let product = catalog!.products[indexPath.row]
            
            productCell.titleLabel.text = product.name
            productCell.availabilityLabel.text = product.stock != 0 ? "Товар в наличии" : "Товар отсутствует"
            productCell.priceLabel.attributedText = product.attributedPrice
            productCell.descriptionLabel.text = product.decodedDescription
            productCell.photoImageView.kf_setImageWithURL(product.pictureURL)
            productCell.photoImageView.kf_showIndicatorWhenLoading = true
            productCell.addtocartButton.alpha = product.stock != 0 ? 1.0 : 0.5
            productCell.addtocartButton.addTarget(self, action: "addToCartButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
            productCell.addtocartButton.tag = indexPath.row
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if indexPath.section == 0
        {
            performSegueToCategory(id: (catalog?.categories[indexPath.row].id)!, title: (catalog?.categories[indexPath.row].name)!)
        }
    }
    
    // MARK: - Navigation
    
    func performSegueToCategory(id categoryID: Int, title: String)
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("catalogVC") as! CatalogViewController
        vc.parentID = categoryID
        vc.title = title
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func performSegueToProduct(id productID: Int)
    {
        
    }
    
    // MARK: - Action
    
    func updateCatalog()
    {
        let path = parentID == 0 ? "engine/sync/m/catalog.php?ID=1" : "engine/sync/m/catalog.php?ID=1&PARENT_ID=\(parentID)"
        Requester.sharedInstance.performRequest(path: path, data: nil)
        { [weak self] (data: NSData?) -> () in
            
            if let strongSelf = self
            {
                if data != nil
                {
                    strongSelf.catalog = CatalogResponseModel(fromData: data!)
                    strongSelf.refreshControl.endRefreshing()
                    strongSelf.tableView.reloadData()
                }
            }
        }
    }
    
    func addToCartButtonPressed(sender: UIButton)
    {
        let product = catalog!.products[sender.tag]
        if product.stock != 0
        {
            DataManager.sharedInstance.addProduct(product)
            
            let alert = UIAlertController(title: nil, message: "Товар добавлен", preferredStyle: UIAlertControllerStyle.Alert)
            presentViewController(alert, animated: true)
            { () -> Void in
                
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue())
                {
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        }
    }
}
