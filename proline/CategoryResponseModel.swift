//
//  CategoryResponseModel.swift
//  proline
//
//  Created by ifau on 19/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class CategoryResponseModel: NSObject
{
    var id: Int!
    var parent: Int?
    var sub: Int?
    var name: String!
    var sort: Int!
    var count: Int?
}