//
//  DataManager.swift
//  proline
//
//  Created by ifau on 19/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import CoreData

class DataManager: NSObject
{
    static let sharedInstance = DataManager()
    var managedObjectContext: NSManagedObjectContext
    
    override init()
    {
        guard let modelURL = NSBundle.mainBundle().URLForResource("DataModel", withExtension:"momd") else
        {
            fatalError("Error loading model from bundle")
        }
        
        guard let mom = NSManagedObjectModel(contentsOfURL: modelURL) else
        {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        self.managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        self.managedObjectContext.persistentStoreCoordinator = psc
        
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        let docURL = urls[urls.endIndex - 1]
        

        let storeURL = docURL.URLByAppendingPathComponent("data.sqlite")
        
        do
        {
            try psc.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: nil)
        }
        catch
        {
            fatalError("Error migrating store: \(error)")
        }
    }
    
    func addProduct(product: ProductResponseModel)
    {
        let id = product.id
        let price = product.price_action ?? product.price_base!
        let name = product.name
        let photo = product.photo
        
        do
        {
            let request = NSFetchRequest(entityName: "ProductEntity")
            request.predicate = NSPredicate(format: "id == %@", argumentArray: [NSNumber(integer: product.id)])
            let fetchedProducts = try managedObjectContext.executeFetchRequest(request) as! [ProductStorageModel]
            
            if fetchedProducts.count != 0
            {
                let entity = fetchedProducts.first!
                entity.quantity = NSNumber(integer: entity.quantity!.integerValue + 1)

                do
                {
                    try managedObjectContext.save()
                }
                catch
                {
                    fatalError("Failure to save context: \(error)")
                }
            }
            else
            {
                let entity = NSEntityDescription.insertNewObjectForEntityForName("ProductEntity", inManagedObjectContext: managedObjectContext) as! ProductStorageModel
                
                entity.id = NSNumber(integer: id)
                entity.price = NSNumber(integer: price)
                entity.quantity = NSNumber(integer: 1)
                entity.name = name
                entity.photo = photo
                
                do
                {
                    try managedObjectContext.save()
                }
                catch
                {
                    fatalError("Failure to save context: \(error)")
                }
            }
        }
        catch
        {
            fatalError("Failure to fetch entity: \(error)")
        }
    }

    func getProducts() -> [ProductStorageModel]
    {
        do
        {
            let request = NSFetchRequest(entityName: "ProductEntity")
            let fetchedProducts = try managedObjectContext.executeFetchRequest(request) as! [ProductStorageModel]
            return fetchedProducts
        }
        catch
        {
            return []
        }
    }
    
    func removeAllProducts()
    {
        do
        {
            let request = NSFetchRequest(entityName: "ProductEntity")
            let fetchedProducts = try managedObjectContext.executeFetchRequest(request) as! [ProductStorageModel]
            for product in fetchedProducts
            {
                managedObjectContext.deleteObject(product)
            }
            
            do
            {
                try managedObjectContext.save()
            }
            catch
            {
                fatalError("Failure to save context: \(error)")
            }
        }
        catch
        {
            fatalError("Failure to fetch entity: \(error)")
        }
    }
    
    func getClientInfo() -> ClientStorageModel?
    {
        do
        {
            let request = NSFetchRequest(entityName: "ClientEntity")
            let fetchedClients = try managedObjectContext.executeFetchRequest(request) as! [ClientStorageModel]
            
            return fetchedClients.count > 0 ? fetchedClients.first! : nil
        }
        catch
        {
            fatalError("Failure to fetch entity: \(error)")
        }
    }
    
    func saveClientInfo(name name: String, email: String, phone: String)
    {
        do
        {
            let request = NSFetchRequest(entityName: "ClientEntity")
            let fetchedClients = try managedObjectContext.executeFetchRequest(request) as! [ClientStorageModel]
            
            if fetchedClients.count != 0
            {
                let client = fetchedClients.first!
                client.name = name
                client.email = email
                client.phone = phone
                
            }
            else
            {
                let client = NSEntityDescription.insertNewObjectForEntityForName("ClientEntity", inManagedObjectContext: managedObjectContext) as! ClientStorageModel
                
                client.name = name
                client.email = email
                client.phone = phone
            }
            
            do
            {
                try managedObjectContext.save()
            }
            catch
            {
                fatalError("Failure to save context: \(error)")
            }
        }
        catch
        {
            fatalError("Failure to fetch entity: \(error)")
        }
    }
}
