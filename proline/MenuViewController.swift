//
//  MenuViewController.swift
//  proline
//
//  Created by ifau on 12/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    
    private let titles = ["Новости", "Оплата", "Доставка", "Контакты"]
    private let files = ["", "pay", "delivery", "contacts"]
    private var footerView : UIView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        let footerLabel = UILabel()
        let footerText = "Приложение разработано компанией «Табус»"
        let footerAttributedText = NSMutableAttributedString(string: footerText)
        footerAttributedText.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(13), range: NSRange(location: 0, length: footerText.characters.count))
        footerAttributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 80.0/255.0, green: 80.0/255.0, blue: 80.0/255.0, alpha: 1.0), range: NSRange(location: 0, length: footerText.characters.count))
        footerAttributedText.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 250.0/255.0, green: 78.0/255.0, blue: 1.0/255.0, alpha: 1.0), range: (footerText as NSString).rangeOfString("«Табус»"))
        
        footerLabel.attributedText = footerAttributedText
        footerLabel.textAlignment = NSTextAlignment.Center
        footerLabel.numberOfLines = 0
        footerLabel.userInteractionEnabled = true
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: "aboutTabusPressed")
        tapRecognizer.numberOfTouchesRequired = 1
        footerLabel.addGestureRecognizer(tapRecognizer)
        
        footerView = footerLabel
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func aboutTabusPressed()
    {
        performSegueWithIdentifier("aboutTabusSegue", sender: nil)
    }
    
    func menuButtonPressed(withIndexPath indexPath: NSIndexPath)
    {
        if indexPath.row == 0
        {
            performSegueWithIdentifier("newsSegue", sender: nil)
        }
        else
        {
            let path = NSBundle.mainBundle().pathForResource(files[indexPath.row], ofType: "html")
            let url = NSURL(fileURLWithPath: path!)
            performSegueWithIdentifier("menuSegue", sender: url)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "menuSegue"
        {
            let dvc = segue.destinationViewController as! WebViewViewController
            let url = sender as! NSURL
            dvc.fileURL = url
        }
    }
    
    // MARK: - UITableViewDelegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return titles.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 44
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        cell.textLabel?.text = titles[indexPath.row]
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        menuButtonPressed(withIndexPath: indexPath)
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView?
    {
        return footerView
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 70
    }
}
