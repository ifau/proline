//
//  NewestResponseModel.swift
//  proline
//
//  Created by ifau on 29/02/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import SWXMLHash

class NewestResponseModel: NSObject
{
    var categories: [CategoryResponseModel]?
    var products: [ProductResponseModel]?
    var page: Int?
    var pages: Int?
    
    init(fromData data: NSData)
    {
        let xml = SWXMLHash.parse(data)
        
        if (xml["cml"]["newest"]["categories"].element?.text != nil && xml["cml"]["newest"]["categories"].element?.text?.characters.count > 1)
        {
            categories = []
            
            for element in xml["cml"]["newest"]["categories"]["category"]
            {
                let category = CategoryResponseModel()
                
                category.id = Int((element["id"].element?.text)!)
                category.name = (element["name"].element?.text)!
                category.sort = Int((element["sort"].element?.text)!)
                category.count = Int((element["count"].element?.text)!)
                
                categories!.append(category)
            }
        }
        
        if (xml["cml"]["newest"].element?.attributes["page"] != nil && xml["cml"]["newest"].element?.attributes["pages"] != nil)
        {
            page = Int((xml["cml"]["newest"].element?.attributes["page"]!)!)
            pages = Int((xml["cml"]["newest"].element?.attributes["pages"]!)!)
        }
        
        if (xml["cml"]["newest"]["products"].element?.text != nil && xml["cml"]["newest"]["products"].element?.text?.characters.count > 1)
        {
            products = []
            
            for element in xml["cml"]["newest"]["products"]["product"]
            {
                let product = ProductResponseModel()
                
                if (element["id"].element?.text == nil) || (element["category"].element?.text == nil) || (element["name"].element?.text == nil) || (element["articul"].element?.text == nil) || (element["stock"].element?.text == nil) || (element["price"]["base"]["cost"].element?.text == nil)
                {
                    continue
                }
                
                product.id = Int((element["id"].element?.text)!)
                product.category = Int((element["category"].element?.text)!)
                product.name = (element["name"].element?.text)!
                product.articul = (element["articul"].element?.text)!
                product.stock = Int((element["stock"].element?.text)!)
                
                if (element["photo"].element != nil)
                {
                    product.photo = (element["photo"].element?.text)!
                }
                
                product.describing = (element["description"].element?.text)!
                product.price_base = Int(Double((element["price"]["base"]["cost"].element?.text)!)!)
                product.price_base_currency = (element["price"]["base"]["currency"].element?.text)!
                
                if (element["price"]["action"]["cost"].element != nil)
                {
                    product.price_action = Int(Double((element["price"]["action"]["cost"].element?.text)!)!)
                    product.price_action_currency = (element["price"]["action"]["currency"].element?.text)!
                }
                
                products!.append(product)
            }
        }
    }
}
