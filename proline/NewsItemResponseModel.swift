//
//  NewsItemResponseModel.swift
//  proline
//
//  Created by ifau on 23/02/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class NewsItemResponseModel: NSObject
{
    var id: Int!
    var name: String!
    var date: String!
    var preview: String!
    var detail: String?
    
    var contentHeight: CGFloat = 0.0
}
