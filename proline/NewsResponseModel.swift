//
//  NewsResponseModel.swift
//  proline
//
//  Created by ifau on 23/02/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import SWXMLHash

class NewsResponseModel: NSObject
{
    var items: [NewsItemResponseModel] = []
    var page: Int!
    var pages: Int!
    
    init(fromData data: NSData)
    {
        let xml = SWXMLHash.parse(data)
        
        page = Int((xml["cml"]["news"].element?.attributes["page"]!)!)
        pages = Int((xml["cml"]["news"].element?.attributes["pages"]!)!)
        
        for element in xml["cml"]["news"]["item"]
        {
            let item = NewsItemResponseModel()
            
            item.id = Int((element["id"].element?.text)!)
            item.name = (element["name"].element?.text)!
            item.date = (element["date"].element?.text)!
            item.preview = (element["preview"].element?.text)!
            
            if (element["detail"].element?.text != nil && element["detail"].element?.text?.characters.count > 1)
            {
                item.detail = (element["detail"].element?.text)!
            }
            
            items.append(item)
        }
    }
}
