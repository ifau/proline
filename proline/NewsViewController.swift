//
//  NewsViewController.swift
//  proline
//
//  Created by ifau on 23/02/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate
{
    @IBOutlet var tableView: UITableView!
    
    private var dividedItems : [[NewsItemResponseModel]] = []
    private var pages = 0
    private var nextPage = 1
    private var isLoading = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        loadNews()
    }
    
    func loadNews()
    {
        isLoading = true
        let path = "engine/sync/m/news.php?ID=1&PAGE_NUM=\(nextPage)"
        Requester.sharedInstance.performRequest(path: path, data: nil)
        { [weak self] (data: NSData?) -> () in
            
            if let strongSelf = self
            {
                if data != nil
                {
                    let response = NewsResponseModel(fromData: data!)
                    strongSelf.appendItems(response.items)
                    
                    strongSelf.pages = response.pages
                    if strongSelf.nextPage < strongSelf.pages
                    {
                        strongSelf.nextPage += 1
                    }
                }
                strongSelf.isLoading = false
            }
        }
    }
    
    func appendItems(items: [NewsItemResponseModel])
    {
        var indexPaths : [NSIndexPath] = []
        let lastSectionIndex = dividedItems.count
        
        tableView.beginUpdates()
        
        for item in items
        {
            var itemDidAppend = false
            if dividedItems.count > 0
            {
                for index in 0...dividedItems.count - 1
                {
                    for groupItem in dividedItems[index]
                    {
                        if item.date == groupItem.date
                        {
                            dividedItems[index].append(item)
                            let indexPath = NSIndexPath(forRow: (dividedItems[index].count - 1), inSection: (index))
                            indexPaths.append(indexPath)
                            itemDidAppend = true
                            break
                        }
                    }
                }
            }
            
            if !itemDidAppend
            {
                dividedItems.append([item])
                let indexPath = NSIndexPath(forRow: 0, inSection: (dividedItems.count - 1))
                indexPaths.append(indexPath)
            }
        }
        
        var indexPathsForUpdate : [NSIndexPath] = []
        let sectionIndexesForUpdate = NSMutableIndexSet()
        for indexPath in indexPaths
        {
            if indexPath.section < lastSectionIndex
            {
                indexPathsForUpdate.append(indexPath)
            }
            else
            {
                sectionIndexesForUpdate.addIndex(indexPath.section)
            }
            
        }
        
        if indexPathsForUpdate.count > 0
        {
            tableView.insertRowsAtIndexPaths(indexPathsForUpdate, withRowAnimation: UITableViewRowAnimation.Automatic)
        }
        if sectionIndexesForUpdate.count > 0
        {
            tableView.insertSections(sectionIndexesForUpdate, withRowAnimation: UITableViewRowAnimation.Automatic)
        }
        tableView.endUpdates()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if segue.identifier == "webviewSegue"
        {
            let dvc = segue.destinationViewController as! WebViewViewController
            let html = sender as! String
            dvc.htmlText = html
        }
    }
    
    // MARK: - UITableViewDelegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return dividedItems.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return dividedItems[section].count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        let item = dividedItems[indexPath.section][indexPath.row]
        return item.contentHeight == 0.0 ? 44 : item.contentHeight
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        let _cell = cell as! NewsTableViewCell
        let item = dividedItems[indexPath.section][indexPath.row]
        
        _cell.previewWebView.tag = (indexPath.section * 1000) + indexPath.row
        _cell.previewWebView.delegate = self
        _cell.previewWebView.userInteractionEnabled = false
        _cell.previewWebView.loadHTMLString("<h2>\(item.name)</h2>\(item.preview)", baseURL: nil)
        _cell.previewWebView.opaque = false
        _cell.previewWebView.backgroundColor = UIColor.clearColor()
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String?
    {
        return dividedItems[section].first!.date
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let item = dividedItems[indexPath.section][indexPath.row]
        performSegueWithIdentifier("webviewSegue", sender: item.detail ?? item.preview)
    }

    // MARK: - UIWebViewDelegate
    
    func webViewDidFinishLoad(webView: UIWebView)
    {
        let row = webView.tag % 1000
        let section = webView.tag / 1000
        let item = dividedItems[section][row]
        
        if (item.contentHeight != 0.0)
        {
            return
        }
        
        item.contentHeight = webView.scrollView.contentSize.height
        tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: row, inSection: section)], withRowAnimation: .Automatic)
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(scrollView: UIScrollView)
    {
        if !isLoading
        {
            let currentOffset = scrollView.contentOffset.y
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            let deltaOffset = maximumOffset - currentOffset
            
            if deltaOffset <= 0
            {
                loadNews()
            }
        }
    }
}
