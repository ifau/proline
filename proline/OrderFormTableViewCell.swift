//
//  OrderFormTableViewCell.swift
//  proline
//
//  Created by ifau on 26/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class OrderFormTableViewCell: UITableViewCell
{
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var textField: UITextField!
}
