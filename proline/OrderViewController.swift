//
//  OrderViewController.swift
//  proline
//
//  Created by ifau on 26/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import JGProgressHUD

class OrderViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    
    private var name: String = ""
    private var email: String = ""
    private var phone: String = ""
    private var comment: String = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if let info = DataManager.sharedInstance.getClientInfo()
        {
            name = info.name!
            email = info.email!
            phone = info.phone!
        }
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    // MARK: - UITableViewDelegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 4
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 44
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        let orderCell = cell as! OrderFormTableViewCell
        orderCell.textField.tag = indexPath.row
        orderCell.textField.addTarget(self, action:"textFieldValueChanged:", forControlEvents:.EditingChanged);
        
        if indexPath.row == 0
        {
            orderCell.titleLabel.text = "Ваше имя*"
            orderCell.textField.text = name
            orderCell.textField.keyboardType = UIKeyboardType.Default
        }
        else if indexPath.row == 1
        {
            orderCell.titleLabel.text = "Ваш E-mail*"
            orderCell.textField.text = email
            orderCell.textField.keyboardType = UIKeyboardType.EmailAddress
        }
        else if indexPath.row == 2
        {
            orderCell.titleLabel.text = "Ваш телефон*"
            orderCell.textField.text = phone
            orderCell.textField.keyboardType = UIKeyboardType.PhonePad
        }
        else if indexPath.row == 3
        {
            orderCell.titleLabel.text = "Комментарий"
            orderCell.textField.text = comment
            orderCell.textField.keyboardType = UIKeyboardType.Default
        }
        else
        {
            orderCell.titleLabel.text = ""
            orderCell.textField.text = ""
            orderCell.textField.keyboardType = UIKeyboardType.Default
        }
    }
    
    // MARK: - UITextField Handler
    
    func textFieldValueChanged(textField: UITextField)
    {
        if textField.tag == 0
        {
            name = textField.text!
        }
        else if textField.tag == 1
        {
            email = textField.text!
        }
        else if textField.tag == 2
        {
            phone = textField.text!
        }
        else if textField.tag == 3
        {
            comment = textField.text!
        }
    }
    
    // MARK: - Action
    
    @IBAction func orderButtonPressed(sender: AnyObject)
    {
        var message: String?
        
        if name.characters.count < 1
        {
            message = "Поле имя не заполнено"
        }
        else if email.characters.count < 3 && !email.characters.contains("@")
        {
            message = "Введён некорректный e-mail адрес"
        }
        else if phone.characters.count < 5
        {
            message = "Введён некорректный номер телефона"
        }
        
        if message != nil
        {
            let alertController = UIAlertController(title: nil, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.Cancel, handler: nil))
            presentViewController(alertController, animated: true, completion: nil)
        }
        else
        {
            DataManager.sharedInstance.saveClientInfo(name: name, email: email, phone: phone)
            or()
        }
    }
    
    func or()
    {
        var clientEncodedString = ""
        clientEncodedString += "ORDER=Y"
        clientEncodedString += "&NAME=\(name.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!)"
        clientEncodedString += "&EMAIL=\(email.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!)"
        clientEncodedString += "&TELEPHONE=\(phone.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!)"
        clientEncodedString += "&COMMENT=\(comment.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!)"
        
        let products = DataManager.sharedInstance.getProducts()
        var productsEncodedString = ""
        for (index, product) in products.enumerate()
        {
            productsEncodedString += "&BASKET%5B\(index)%5D%5BPRODUCT_ID%5D=\(product.id!.integerValue)&BASKET%5B\(index)%5D%5BQUANTITY%5D=\(product.quantity!.integerValue)"
        }
        
        let queryString = "\(clientEncodedString)\(productsEncodedString)"
        
        let request = NSMutableURLRequest(URL: NSURL(string: "http://www.proline-rus.ru/engine/sync/m/order.php")!)
        request.HTTPMethod = "POST"
        request.HTTPBody = queryString.dataUsingEncoding(NSUTF8StringEncoding)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        let hud = JGProgressHUD(style: JGProgressHUDStyle.Dark)
        hud.showInView(self.view.window, animated: true)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request)
        { data, response, error in
            
            dispatch_async(dispatch_get_main_queue(),
            { [unowned self] () -> Void in
                
                hud.dismiss()
                guard error == nil && data != nil else
                {
                    let alertController = UIAlertController(title: nil, message: error?.localizedDescription, preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                    
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200
                {
                        
                    let alertController = UIAlertController(title: nil, message: "Не удалось отправить заказ", preferredStyle: UIAlertControllerStyle.Alert)
                    alertController.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.Cancel, handler: nil))
                    self.presentViewController(alertController, animated: true, completion: nil)
                    
                    return
                }
                
                let alertController = UIAlertController(title: nil, message: "Заказ успешно отправлен", preferredStyle: UIAlertControllerStyle.Alert)
                
                alertController.addAction(UIAlertAction(title: "Ок", style: UIAlertActionStyle.Cancel, handler:
                { [unowned self] (UIAlertAction) -> Void in
                        
                    DataManager.sharedInstance.removeAllProducts()
                    self.navigationController?.popViewControllerAnimated(true)
                }))
                
                self.presentViewController(alertController, animated: true, completion: nil)
            })
        }
        
        task.resume()
    }
}
