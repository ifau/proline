//
//  ProductResponseModel.swift
//  proline
//
//  Created by ifau on 19/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class ProductResponseModel: NSObject
{
    var id: Int!
    var category: Int!
    var name: String!
    var articul: String!
    var sort: Int?
    var stock: Int!
    var photo: String?
    var describing: String!
    var price_base: Int!
    var price_base_currency: String!
    var price_action: Int?
    var price_action_currency: String?
    
    var attributedPrice: NSAttributedString
    {
        get
        {
            let string = NSMutableAttributedString()
            
            let price_string = "\(price_base) руб."
            
            let str = NSMutableAttributedString(string: price_string)
            str.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(15), range: NSRange(location: 0, length: price_string.characters.count - 1))
            str.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 1.0), range: NSRange(location: 0, length: price_string.characters.count))
            
            if price_action != nil
            {
                str.addAttribute(NSStrikethroughStyleAttributeName, value: NSUnderlineStyle.StyleSingle.rawValue, range: NSRange(location: 0, length: price_string.characters.count))
            }
            
            string.appendAttributedString(str)
            
            
            if price_action != nil
            {
                let price_string = " \(price_action!) руб."
                
                let str = NSMutableAttributedString(string: price_string)
                str.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(15), range: NSRange(location: 1, length: price_string.characters.count - 1))
                str.addAttribute(NSForegroundColorAttributeName, value: UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0), range: NSRange(location: 1, length: price_string.characters.count - 1))
                
                string.appendAttributedString(str)
            }
            
            return string
        }
    }
    
    var pictureURL: NSURL
    {
        get
        {
            return NSURL(string: "http://www.proline-rus.ru\(photo!)")!
        }
    }
    
    var decodedDescription: String
    {
        get
        {
            let encodedData = describing.dataUsingEncoding(NSUTF8StringEncoding)!
            let attributedOptions : [String: AnyObject] =
            [
                NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                NSCharacterEncodingDocumentAttribute: NSUTF8StringEncoding
            ]
            
            var attributedString: NSAttributedString?
            
            do
            {
                attributedString = try NSAttributedString(data: encodedData, options: attributedOptions, documentAttributes: nil)
                return attributedString!.string
            }
            catch
            {
                return describing
            }
        }
    }
}
