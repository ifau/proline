//
//  ProductTableViewCell.swift
//  proline
//
//  Created by ifau on 18/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell
{
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var availabilityLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var addtocartButton: UIButton!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        addtocartButton.layer.borderWidth = 1
        addtocartButton.layer.borderColor = UIColor.blackColor().CGColor
    }
}