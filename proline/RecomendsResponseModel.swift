//
//  RecomendsResponseModel.swift
//  proline
//
//  Created by ifau on 01/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import SWXMLHash

class RecomendsResponseModel: NSObject
{
    var categories: [CategoryResponseModel]?
    var products: [ProductResponseModel]?
    var page: Int?
    var pages: Int?
    
    init(fromData data: NSData)
    {
        let xml = SWXMLHash.parse(data)
        
        if (xml["cml"]["recommends"]["categories"].element?.text != nil && xml["cml"]["recommends"]["categories"].element?.text?.characters.count > 1)
        {
            categories = []
            
            for element in xml["cml"]["recommends"]["categories"]["category"]
            {
                let category = CategoryResponseModel()
                
                category.id = Int((element["id"].element?.text)!)
                category.name = (element["name"].element?.text)!
                category.sort = Int((element["sort"].element?.text)!)
                category.count = Int((element["count"].element?.text)!)
                
                categories!.append(category)
            }
        }
        
        if (xml["cml"]["recommends"].element?.attributes["page"] != nil && xml["cml"]["recommends"].element?.attributes["pages"] != nil)
        {
            page = Int((xml["cml"]["recommends"].element?.attributes["page"]!)!)
            pages = Int((xml["cml"]["recommends"].element?.attributes["pages"]!)!)
        }
        
        if (xml["cml"]["recommends"]["products"].element?.text != nil && xml["cml"]["recommends"]["products"].element?.text?.characters.count > 1)
        {
            products = []
            
            for element in xml["cml"]["recommends"]["products"]["product"]
            {
                let product = ProductResponseModel()
                
                if (element["id"].element?.text == nil) || (element["category"].element?.text == nil) || (element["name"].element?.text == nil) || (element["articul"].element?.text == nil) || (element["stock"].element?.text == nil) || (element["price"]["base"]["cost"].element?.text == nil)
                {
                    continue
                }
                
                product.id = Int((element["id"].element?.text)!)
                product.category = Int((element["category"].element?.text)!)
                product.name = (element["name"].element?.text)!
                product.articul = (element["articul"].element?.text)!
                product.stock = Int((element["stock"].element?.text)!)
                
                if (element["photo"].element != nil)
                {
                    product.photo = (element["photo"].element?.text)!
                }
                
                product.describing = (element["description"].element?.text)!
                product.price_base = Int(Double((element["price"]["base"]["cost"].element?.text)!)!)
                product.price_base_currency = (element["price"]["base"]["currency"].element?.text)!
                
                if (element["price"]["action"]["cost"].element != nil)
                {
                    product.price_action = Int(Double((element["price"]["action"]["cost"].element?.text)!)!)
                    product.price_action_currency = (element["price"]["action"]["currency"].element?.text)!
                }
                
                products!.append(product)
            }
        }
    }
}
