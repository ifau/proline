//
//  RecomendsViewController.swift
//  proline
//
//  Created by ifau on 01/03/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class RecomendsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet var tableView: UITableView!
    
    private var categories : [CategoryResponseModel]?
    private var products : [ProductResponseModel] = []
    
    var parentID : Int = 0
    private var pages = 0
    private var nextPage = 1
    private var isLoading = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        updateCatalog()
    }
    
    // MARK: - UITableViewDelegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if categories != nil
        {
            return categories!.count
        }
        else
        {
            return products.count
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        if products.count > 0
        {
            let product = products[indexPath.row]
            let desc = product.describing as NSString!
            
            let sizeOfDesc = desc.boundingRectWithSize(
                CGSizeMake(self.view.bounds.size.width - 18, CGFloat.infinity),
                options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont.systemFontOfSize(15)],
                context: nil).size
            
            return 164 + sizeOfDesc.height
        }
        else
        {
            return 44.0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let identifier = categories != nil ? "Cell" : "ProductCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(identifier, forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if categories != nil
        {
            let category = categories![indexPath.row]
            cell.textLabel?.text = "\(category.name) (\(category.count!))"
        }
        else
        {
            let productCell = cell as! ProductTableViewCell
            let product = products[indexPath.row]
            
            productCell.titleLabel.text = product.name
            productCell.availabilityLabel.text = product.stock != 0 ? "Товар в наличии" : "Товар отсутствует"
            productCell.priceLabel.attributedText = product.attributedPrice
            productCell.descriptionLabel.text = product.decodedDescription
            productCell.photoImageView.kf_setImageWithURL(product.pictureURL)
            productCell.photoImageView.kf_showIndicatorWhenLoading = true
            productCell.addtocartButton.alpha = product.stock != 0 ? 1.0 : 0.5
            productCell.addtocartButton.addTarget(self, action: "addToCartButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
            productCell.addtocartButton.tag = indexPath.row
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        if categories != nil
        {
            performSegueToCategory(id: (categories![indexPath.row].id)!, title: (categories![indexPath.row].name)!)
        }
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(scrollView: UIScrollView)
    {
        if !isLoading
        {
            let currentOffset = scrollView.contentOffset.y
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            let deltaOffset = maximumOffset - currentOffset
            
            if deltaOffset <= 0
            {
                if nextPage < pages
                {
                    updateCatalog()
                }
            }
        }
    }
    
    // MARK: - Navigation
    
    func performSegueToCategory(id categoryID: Int, title: String)
    {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("recommendsVC") as! RecomendsViewController
        vc.parentID = categoryID
        vc.title = title
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func performSegueToProduct(id productID: Int)
    {
        
    }
    
    // MARK: - Action
    
    func updateCatalog()
    {
        isLoading = true
        let path = parentID == 0 ? "engine/sync/m/recommends.php?ID=1" : "engine/sync/m/recommends.php?ID=1&PARENT_ID=\(parentID)&PAGE_NUM=\(nextPage)"
        Requester.sharedInstance.performRequest(path: path, data: nil)
        { [weak self] (data: NSData?) -> () in
            
            if let strongSelf = self
            {
                if data != nil
                {
                    let response = RecomendsResponseModel(fromData: data!)
                    if response.categories != nil
                    {
                        strongSelf.categories = response.categories
                        strongSelf.tableView.reloadData()
                    }
                    else
                    {
                        strongSelf.appendItems(response.products!)
                        
                        strongSelf.pages = response.pages!
                        if strongSelf.nextPage < strongSelf.pages
                        {
                            strongSelf.nextPage += 1
                        }
                    }
                }
                strongSelf.isLoading = false
            }
        }
    }
    
    func appendItems(items: [ProductResponseModel])
    {
        var indexPaths : [NSIndexPath] = []
        for item in items
        {
            products.append(item)
            let index = products.indexOf(item)
            let indexPath = NSIndexPath(forRow: index!, inSection: 0)
            indexPaths.append(indexPath)
        }
        
        tableView.beginUpdates()
        tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.Automatic)
        tableView.endUpdates()
    }
    
    func addToCartButtonPressed(sender: UIButton)
    {
        let product = products[sender.tag]
        if product.stock != 0
        {
            DataManager.sharedInstance.addProduct(product)
            
            let alert = UIAlertController(title: nil, message: "Товар добавлен", preferredStyle: UIAlertControllerStyle.Alert)
            presentViewController(alert, animated: true)
            { () -> Void in
                    
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue())
                {
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        }
    }
}
