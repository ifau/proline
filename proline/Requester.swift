//
//  Requester.swift
//  proline
//
//  Created by ifau on 27/12/15.
//  Copyright © 2015 tabus. All rights reserved.
//

import UIKit

class Requester: NSObject
{
    static let sharedInstance = Requester()
    var requestsCount = 0
    
    func performRequest(path path: String, data: NSData?, completion: (NSData?) -> ())
    {
        let session = NSURLSession.sharedSession()
        let str = "http://www.proline-rus.ru/\(path)"
        let url = NSURL(string: str)!
        let request = NSMutableURLRequest(URL: url)
        
        if data != nil
        {
            request.HTTPBody = data
            request.HTTPMethod = "POST"
            //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        else
        {
            request.HTTPMethod = "GET"
        }
        
        requestsCount += 1
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        let task = session.dataTaskWithRequest(request) { [unowned self] (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            self.requestsCount -= 1
            UIApplication.sharedApplication().networkActivityIndicatorVisible = self.requestsCount == 0 ? false : true
            
            if error == nil && data != nil
            {
                dispatch_async(dispatch_get_main_queue(), { completion(data) })
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), { completion(nil) })
            }
        }
        
        task.resume()
    }
}
