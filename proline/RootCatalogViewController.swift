//
//  RootCatalogViewController.swift
//  proline
//
//  Created by ifau on 27/02/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class RootCatalogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var tableView: UITableView!
    private let titles = ["Каталог товаров", "Поиск", "Акции", "Новинки", "Рекомендации"]
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func menuButtonPressed(withIndexPath indexPath: NSIndexPath)
    {
        if indexPath.row == 0
        {
            performSegueWithIdentifier("catalogSegue", sender: nil)
        }
        else if indexPath.row == 1
        {
            performSegueWithIdentifier("searchSegue", sender: nil)
        }
        else if indexPath.row == 2
        {
            performSegueWithIdentifier("promosSegue", sender: nil)
        }
        else if indexPath.row == 3
        {
            performSegueWithIdentifier("newestSegue", sender: nil)
        }
        else if indexPath.row == 4
        {
            performSegueWithIdentifier("recommendsSegue", sender: nil)
        }
    }
    
    // MARK: - UITableViewDelegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return titles.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 44
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        cell.textLabel?.text = titles[indexPath.row]
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        menuButtonPressed(withIndexPath: indexPath)
    }
}
