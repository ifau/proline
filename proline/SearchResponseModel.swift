//
//  SearchResponseModel.swift
//  proline
//
//  Created by ifau on 26/02/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit
import SWXMLHash

class SearchResponseModel: NSObject
{
    var items: [ProductResponseModel] = []
    var page: Int?
    var pages: Int?
    
    init(fromData data: NSData)
    {
        let xml = SWXMLHash.parse(data)
        
        if (xml["cml"]["search"].element?.attributes["page"] != nil && xml["cml"]["search"].element?.attributes["pages"] != nil)
        {
            page = Int((xml["cml"]["search"].element?.attributes["page"]!)!)
            pages = Int((xml["cml"]["search"].element?.attributes["pages"]!)!)
            
            for element in xml["cml"]["search"]["products"]["product"]
            {
                let product = ProductResponseModel()
                
                if (element["id"].element?.text == nil) || (element["category"].element?.text == nil) || (element["name"].element?.text == nil) || (element["articul"].element?.text == nil) || (element["stock"].element?.text == nil) || (element["price"]["base"]["cost"].element?.text == nil)
                {
                    continue
                }
                
                product.id = Int((element["id"].element?.text)!)
                product.category = Int((element["category"].element?.text)!)
                product.name = (element["name"].element?.text)!
                product.articul = (element["articul"].element?.text)!
                //product.sort = Int((element["sort"].element?.text)!)
                product.stock = Int((element["stock"].element?.text)!)
                
                if (element["photo"].element != nil)
                {
                    product.photo = (element["photo"].element?.text)!
                }
                
                product.describing = (element["description"].element?.text)!
                product.price_base = Int(Double((element["price"]["base"]["cost"].element?.text)!)!)
                product.price_base_currency = (element["price"]["base"]["currency"].element?.text)!
                
                if (element["price"]["action"]["cost"].element != nil)
                {
                    product.price_action = Int(Double((element["price"]["action"]["cost"].element?.text)!)!)
                    product.price_action_currency = (element["price"]["action"]["currency"].element?.text)!
                }
                
                items.append(product)
            }
        }
    }
}
