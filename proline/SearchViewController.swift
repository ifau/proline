//
//  SearchViewController.swift
//  proline
//
//  Created by ifau on 25/02/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating, UISearchBarDelegate
{
    @IBOutlet var tableView: UITableView!
    
    private let searchController = UISearchController(searchResultsController: nil)
    private var products : [ProductResponseModel] = []
    
    private var pages = 0
    private var nextPage = 1
    private var isLoading = false
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.scopeButtonTitles = ["Везде", "По описанию", "По артикулу"]
        searchController.searchBar.delegate = self
        
        tableView.tableHeaderView = searchController.searchBar
        
        definesPresentationContext = true
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        searchController.active = true
    }
    
    func searchProducts(requestString: String, scope: String)
    {
        isLoading = true
        let encodedString = requestString.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!
        var mode = 3
        if scope == "По описанию"
        {
            mode = 1
        }
        else if scope == "По артикулу"
        {
            mode = 2
        }
        
        let path = "engine/sync/m/search.php?ID=1&FIND=\(encodedString)&PAGE_NUM=\(nextPage)&MODE=\(mode)"
        Requester.sharedInstance.performRequest(path: path, data: nil)
        { [weak self] (data: NSData?) -> () in
            
            if let strongSelf = self
            {
                if data != nil
                {
                    let response = SearchResponseModel(fromData: data!)
                    if response.pages != nil
                    {
                        strongSelf.appendItems(response.items)
                        
                        strongSelf.pages = response.pages!
                        if strongSelf.nextPage < strongSelf.pages
                        {
                            strongSelf.nextPage += 1
                        }
                    }
                }
                strongSelf.isLoading = false
            }
        }
    }
    
    func appendItems(items: [ProductResponseModel])
    {
        var indexPaths : [NSIndexPath] = []
        for item in items
        {
            products.append(item)
            let index = products.indexOf(item)
            let indexPath = NSIndexPath(forRow: index!, inSection: 0)
            indexPaths.append(indexPath)
        }
        
        tableView.beginUpdates()
        tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.Automatic)
        tableView.endUpdates()
    }
    
    func cleanResults()
    {
        pages = 0
        nextPage = 1
        products.removeAll()
        tableView.reloadData()
    }
    
    func addToCartButtonPressed(sender: UIButton)
    {
        let product = products[sender.tag]
        if product.stock != 0
        {
            DataManager.sharedInstance.addProduct(product)
            
            let alert = UIAlertController(title: nil, message: "Товар добавлен", preferredStyle: UIAlertControllerStyle.Alert)
            presentViewController(alert, animated: true)
            { () -> Void in
                    
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(1 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_main_queue())
                {
                    alert.dismissViewControllerAnimated(true, completion: nil)
                }
            }
        }
    }
    
    // MARK: - UITableViewDelegate
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return products.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath)
    {
        let product = products[indexPath.row]
        let productCell = cell as! ProductTableViewCell
        
        productCell.titleLabel.text = product.name
        productCell.availabilityLabel.text = product.stock != 0 ? "Товар в наличии" : "Товар отсутствует"
        productCell.priceLabel.attributedText = product.attributedPrice
        productCell.descriptionLabel.text = product.decodedDescription
        productCell.photoImageView.kf_setImageWithURL(product.pictureURL)
        productCell.photoImageView.kf_showIndicatorWhenLoading = true
        productCell.addtocartButton.alpha = product.stock != 0 ? 1.0 : 0.5
        productCell.addtocartButton.addTarget(self, action: "addToCartButtonPressed:", forControlEvents: UIControlEvents.TouchUpInside)
        productCell.addtocartButton.tag = indexPath.row
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        let product = products[indexPath.row]
        let desc = product.describing as NSString!
            
        let sizeOfDesc = desc.boundingRectWithSize(
                CGSizeMake(self.view.bounds.size.width - 18, CGFloat.infinity),
                options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                attributes: [NSFontAttributeName: UIFont.systemFontOfSize(15)],
                context: nil).size
            
        return 164 + sizeOfDesc.height
    }
    
    // MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(scrollView: UIScrollView)
    {
        if !isLoading
        {
            let currentOffset = scrollView.contentOffset.y
            let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
            let deltaOffset = maximumOffset - currentOffset
            
            if deltaOffset <= 0
            {
                if nextPage < pages
                {
                    let searchText = searchController.searchBar.text!
                    let scope = searchController.searchBar.scopeButtonTitles![searchController.searchBar.selectedScopeButtonIndex]
                    searchProducts(searchText, scope: scope)
                }
            }
        }
    }
    
    // MARK: - UISearchResultsUpdating
    
    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar)
    {
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText(searchController.searchBar.text!, scope: scope)
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar)
    {
        cleanResults()
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "Везде")
    {
        cleanResults()
        searchProducts(searchText, scope: scope)
    }
    
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int)
    {
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}
