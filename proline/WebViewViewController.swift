//
//  WebViewViewController.swift
//  proline
//
//  Created by ifau on 12/01/16.
//  Copyright © 2016 tabus. All rights reserved.
//

import UIKit

class WebViewViewController: UIViewController, UIWebViewDelegate
{
    var fileURL : NSURL?
    var htmlText: String?
    
    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        webView.delegate = self
        
        if fileURL != nil
        {
            webView.loadRequest(NSURLRequest(URL: fileURL!))
        }
        else if htmlText != nil
        {
            webView.loadHTMLString(htmlText!, baseURL: NSURL(string: "http://www.proline-rus.ru")!)
        }
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
        if navigationType == UIWebViewNavigationType.LinkClicked
        {
            UIApplication.sharedApplication().openURL(request.URL!)
            return false
        }
        return true
    }
}
